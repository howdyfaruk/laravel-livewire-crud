<?php


namespace App\Http\Livewire;
use Illuminate\Pagination\Paginator;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

use App\Models\User;


class Users extends Component{

    use WithPagination, WithFileUploads;

    public $currentPage = 1;

    public $search = "";
    public $image = "";
    public $filter = 5;

    public  $name, $email, $user_id;
 
    public $updateMode = false;


    public function render(){

        // $this->users = User::latest()->get();

        return view('livewire.users',[
            'totalUsers'  => User::all()->count(),
            'users'  => User::latest()->where('name', 'LIKE', "%{$this->search}%")->orWhere('email', 'LIKE', "%{$this->search}%")->paginate($this->filter),
        ]);

        // return view('livewire.users');

    }

    public function setPage($url){

        $this->currentPage = explode('page=',$url)[1];

        Paginator::currentPageResolver(function(){
            return $this->currentPage;
        });
    }

    private function resetInputFields(){
        $this->name = '';
        $this->email = '';
        $this->image = '';
    }


    public function store(){

        $validatedData = $this->validate([
            'name'      => 'required',
            'email'     => 'required|email',
            'image'     => 'required',
        ]);

        $filename = $this->image->store('images','public');

        $validatedData['image'] = $filename;

        User::create($validatedData);

        session()->flash('message', 'Users Created Successfully.');

        $this->resetInputFields();

        $this->emit('userStore'); // Close model to using to jquery
        $this->emit('alert');

    }


    public function edit($id){

        $this->updateMode = true;

        $user = User::find($id);

        $this->user_id = $id;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->image = $user->image;
    }


    public function cancel(){

        $this->updateMode = false;

        $this->resetInputFields();
    }


    public function update(){

        $validatedDate = $this->validate([
            'name' => 'required',
            'email' => 'required|email',
        ]);

        if ($this->user_id) {
            $user = User::find($this->user_id);

            $user->update([
                'name' => $this->name,
                'email' => $this->email,
            ]);

            $this->updateMode = false;

            session()->flash('message', 'Users Updated Successfully.');

            $this->resetInputFields();

            $this->emit('alert');
        }
    }


    public function delete($id){

        if($id){

            User::where('id',$id)->delete();

            session()->flash('message', 'Users Deleted Successfully.');

            $this->emit('alert');
        }
    }
}