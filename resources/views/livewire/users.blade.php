<div>
    <style>
       

    </style>
    @include('livewire.create')

    @include('livewire.update')

    @if (session()->has('message'))
        <div class="alert alert-success">
          {{ session('message') }}
        </div>
    @endif
    <div class="row mt-3">
        <div class="col-md-2">
            <select class="form-control" name="filter" id="filter" wire:model.lazy="filter">
                {{-- <option selected disabled>filter</option> --}}
                <option value="5" selected>5</option>
                <option value="10">10</option>
                <option value="20">20</option>
            </select>
        </div>

        <div class="col-md-6">
        
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <input wire:model.debounce.200="search" class="form-control" type="text" name="search" id="search" placeholder="search">
            </div>
        </div>
    </div>

    <table class="table table-bordered">
        <thead>
            <tr>
                <th>No.
                    {{-- <div>
                        <div class="up-arrow" wire:click="sort('name','ASC')">

                        </div>
                        <div class="down-arrow" wire:click="sort('name','DESC')">

                        </div>
                    </div> --}}
                </th>
                <th>Photo</th>
                <th>Name</th>
                <th>Email</th>
                <th>Action</th>
            </tr>
        </thead>

        <tbody>
            @foreach($users as $i=>$value)
            <tr>
                <td>{{ $users->firstItem()+ $i }}</td>
                <td> <img src="{{ asset('storage/'.$value->image) }}" alt="photo" title="photo" height="60" width="60"></a></td>
                <td>{{ $value->name }}</td>
                <td>{{ $value->email }}</td>
                <td>
                    <div class="btn-group">
                        <button data-toggle="modal" data-target="#updateModal" wire:click="edit({{ $value->id }})" class="btn btn-primary btn-sm">Edit</button>
                        <button wire:click="delete({{ $value->id }})" class="btn btn-danger btn-sm">Delete</button>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-md-4">
            <p class="mt-4">Showing {{ $users->firstItem() ? :'0' }} to {{ $users->lastItem() ? :'0' }} of {{ $totalUsers }} entries</p>
        </div>
        <div class="col-md-8">
           <p> {{ $users->links('livewire.pagination-links') }}</p>
        </div>
    </div>
    
</div>