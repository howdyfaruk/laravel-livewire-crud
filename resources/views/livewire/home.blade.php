<!DOCTYPE html>
<html>
    <head>
        <title>Livewire User Management</title>

        <script src="{{ asset('js/app.js') }}" defer></script>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        @livewireStyles
    </head>

    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card" style="min-height:540px">
                        <div class="card-body">
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session('message') }}
                                </div>
                            @endif
                            @livewire('users')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @livewireScripts

        <script type="text/javascript">

            window.livewire.on('userStore', () => {
                $('#exampleModal').modal('hide');
            });

            window.livewire.on('alert', () => {
            
                window.setTimeout(function() {
                    $(".alert").fadeTo(500, 0).slideUp(500, function(){
                        $(this).remove(); 
                    });
                }, 2000);
            });
        </script>

    </body>
</html>